variable "transit_gateway_route_table_id" {
  default = null
}

variable "transit_gateway_attachment_id" {
  default = null
}

variable "destination_cidr_block" {
  default = null
}

variable "transit_gateway_vpn_attachment" {
  #type = string
  #default = null
}

