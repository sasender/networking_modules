resource "aws_ec2_transit_gateway_route" "example" {
  #destination_cidr_block         = "172.31.32.0/20"
  #destination_cidr_block          = "192.168.0.0/16"
  destination_cidr_block = var.destination_cidr_block
  # transit_gateway_attachment_id  = data.aws_ec2_transit_gateway_vpn_attachment.example.id
  # transit_gateway_route_table_id = "${aws_ec2_transit_gateway_route_table.example.id}"
  transit_gateway_attachment_id = var.transit_gateway_vpn_attachment
  transit_gateway_route_table_id = var.transit_gateway_route_table_id
}


