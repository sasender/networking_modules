resource "aws_ec2_transit_gateway_route_table_association" "vpc_association" {
  transit_gateway_attachment_id  = var.aws_ec2_transit_gateway_vpc_attachment
  #transit_gateway_attachment_id = aws_ec2_transit_gateway_vpc_attachment.id
  #transit_gateway_route_table_id = "${aws_ec2_transit_gateway_route_table.example.id}"
  transit_gateway_route_table_id = var.route_table_id
  #transit_gateway_vpn_attachment  = transit_gateway_vpn_attachment.example.id

}