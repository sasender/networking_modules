variable "transit_gateway_attachment_id" {
  default = null
}

variable "aws_ec2_transit_gateway_route_table" {
  default = null
}

variable "transit_gateway_route_table_id" {
    default   = null
    description = "(optional) describe your variable"
}

variable "destination_cidr_block" {
  default = null
}


