resource "aws_ec2_transit_gateway_route" "vpc_example" {
  #destination_cidr_block         = "172.24.128.0/20"
  destination_cidr_block         = var.destination_cidr_block
#   transit_gateway_attachment_id  = "${aws_ec2_transit_gateway_vpc_attachment.vpc_attachment.id}"
#   transit_gateway_route_table_id = "${aws_ec2_transit_gateway_route_table.example.id}"
transit_gateway_attachment_id = var.transit_gateway_attachment_id
transit_gateway_route_table_id = var.transit_gateway_route_table_id

}
