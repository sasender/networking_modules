variable "route_table_id" {
  default = null
}

variable "vpn_attachment_id" {
 default = null 
}

variable "transit_gateway_attachment_id" {
  default = null
}

variable "transit_gateway_route_table_id" {
  default = null
}


# variable "aws_ec2_transit_gateway_vpn_attachment_id" {
#   default = null
# }
variable "aws_ec2_transit_gateway_vpn_attachment" {
  #default = null
  #type = string
  #   type        = object({
  #   id = string
  #   # other attributes if applicable
  # })
}