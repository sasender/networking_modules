resource "aws_customer_gateway" "example" {
bgp_asn = 65000 # Your on-premises ASN
#ip_address = "13.232.124.188" # Your public IP
ip_address = var.ip_address
type = "ipsec.1"
  tags = {
    Name = "var.tags"
    }
}

