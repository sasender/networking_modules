output "customer_gateway" {
  description = "Map of Customer Gateway attributes"
  value       = aws_customer_gateway.example
}
