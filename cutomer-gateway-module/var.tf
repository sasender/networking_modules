variable "name" {
  default = null
}

variable "ip_address" {
  default = null
}

variable "tags" {
  default = null
}

