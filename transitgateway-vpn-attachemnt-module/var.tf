variable "vpc_id" {
  default = null
}

variable "subnet_ids" {
  default = null
}

variable "aws_ec2_transit_gateway" {
  default = null
}

# variable "transit_gateway_id" {
#   # default = null
#   description = "transitgateway"
# }

variable "vpn_connection_id" {
  default = null
}

variable "subnet_id" {
  default = null
}
# variable "transit_gateway" {
#   #default = null
# }
variable "transit_gateway_id" {
  description = "ID of the Transit Gateway"
  type        = string
  # default = null
}

