module "site-to-site-vpn" {
  source = "./site-to-site-vpn-module"
  name   = var.name
  customer_gateway_id  = module.customer_gateway.customer_gateway.id
  transit_gateway_id = module.transit_gateway.transit_gateway_id
}


