variable "tags" {
  default = null
}

variable "name" {
  default = null
}

variable "transit_gateway_id" {
  description = "ID of the Transit Gateway"
  #type        = string
  default = null
}

variable "route_table_id" {
  default = null
}

variable "transit_gateway_route_table_id" {
  default = null
}




