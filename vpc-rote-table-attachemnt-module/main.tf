resource "aws_route" "transit-gateway_route_public_subnet1" {
#route_table_id = aws_route_table.tera_out.id
route_table_id = var.route_table_id
#destination_cidr_block = "192.168.0.0/16"
destination_cidr_block = var.destination_cidr_block
#transit_gateway_id = aws_ec2_transit_gateway.example.id
transit_gateway_id = var.transit_gateway_id
}
