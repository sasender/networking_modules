# output "transit_gateway_attachment_id" {
#   value = mo
# }

output "route_table_id" {
  value = module.transit_gateway_route_table.route_table_id
}

output "transit_gateway_id" {
  value = module.transit_gateway.transit_gateway_id
}

output "aws_vpn_connection_id" {
  value = module.site-to-site-vpn.aws_vpn_connection_id
}


