module "transit_gateway" {
  source = "./transite-gateway-module"
  name     = var.name
}

module "transit_gateway_route_table" {
  source = "./transit-gateway-route-module"
  name   = var.name
  transit_gateway_id = module.transit_gateway.transit_gateway_id
  #route_table_id = module.transit_gateway_route_table.route_table_id

}


module "aws_ec2_transit_gateway_vpc_attachment" {
  source = "./transitgateway-atttachment-module"
  subnet_ids = ["subnet-0af824f6a0ff11ffb"]
  vpc_id = "vpc-0a0b257b1828381f3"
  transit_gateway_id = module.transit_gateway.transit_gateway_id
}





# module "site-to-site-vpn" {
#   source = "./site-to-site-vpn-module"
#   name   = var.name
#   customer_gateway_id = "13.232.124.188"
#   transit_gateway_id = module.transit_gateway.aws_ec2_transit_gateway

# }


module "aws_ec2_transit_gateway_vpn_attachment" {
  #source = "./transitgateway-atttachment-module"
  source = "./transitgateway-vpn-attachemnt-module"
  subnet_ids = ["subnet-0af824f6a0ff11ffb"]
  #subnet_id = ["subnet-0af824f6a0ff11ffb"]
  vpc_id = "vpc-0a0b257b1828381f3"
  transit_gateway_id = module.transit_gateway.transit_gateway_id
  vpn_connection_id  = module.site-to-site-vpn.aws_vpn_connection_id

}



module "aws_ec2_transit_gateway_route_table_association" {
  source = "./transit-gateway-route-table-vpc-assoiation"
  #transit_gateway_attachment_id = module.aws_ec2_transit_gateway_vpc_attachment.vpc_attachment_id
  #transit_gateway_route_table_id = module.transit_gateway_route_table.route_table_id
  aws_ec2_transit_gateway_vpc_attachment = module.aws_ec2_transit_gateway_vpc_attachment.vpc_attachment_id
  route_table_id = module.transit_gateway_route_table.route_table_id
  

}


module "aws_ec2_transit_gateway_route_table_vpn_association" {
  source = "./transit-route-table-vpn-assoiation-module"
  #transit_gateway_attachment_id = module.aws_ec2_transit_gateway_vpn_attachment.vpn_attachment_id
  transit_gateway_route_table_id = module.transit_gateway_route_table.route_table_id
  aws_ec2_transit_gateway_vpn_attachment = module.aws_ec2_transit_gateway_vpn_attachment.vpn_attachment_id
  route_table_id = module.transit_gateway_route_table.route_table_id
}





module "route_table_propagation" {
  source = "./transite-gateway-propagation-module"
  transit_gateway_attachment_id = module.aws_ec2_transit_gateway_vpc_attachment.vpc_attachment_id
  #transit_gateway_route_table_id = module.transit_gateway_route_table.route_table_id
  aws_ec2_transit_gateway_vpc_attachment = module.aws_ec2_transit_gateway_vpc_attachment.vpc_attachment_id
  route_table_id = module.transit_gateway_route_table.route_table_id

}



module "aws_ec2_transit_gateway_route_table_vpn_proagation" {
  source = "./vpn_propagation_module"
  #transit_gateway_attachment_id = module.aws_ec2_transit_gateway_vpn_attachment.vpn_attachment_id
  transit_gateway_route_table_id = module.transit_gateway_route_table.route_table_id
  aws_ec2_transit_gateway_vpn_attachment = module.aws_ec2_transit_gateway_vpn_attachment.vpn_attachment_id
    route_table_id = module.transit_gateway_route_table.route_table_id
}



module "transite-gate-way-route" {
  source = "./transit-route-allocation-vpc"
  destination_cidr_block = "192.168.0.0/16"
  transit_gateway_attachment_id = module.aws_ec2_transit_gateway_vpc_attachment.vpc_attachment_id
  transit_gateway_route_table_id = module.transit_gateway_route_table.route_table_id
}

module "transit_gateway_vpn_route" {
  source = "./transit-route-vpn-allocation-module"
  destination_cidr_block = "172.24.128.0/20"
  #transit_gateway_attachment_id = module.aws_ec2_transit_gateway_vpn_attachment.vpn_attachment_id
  transit_gateway_route_table_id = module.transit_gateway_route_table.route_table_id
  transit_gateway_vpn_attachment = module.aws_ec2_transit_gateway_vpn_attachment.vpn_attachment_id
}







