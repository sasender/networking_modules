variable "transit_gateway_id" {
  description = "ID of the Transit Gateway"
  #type        = string
  default = null
}

variable "route_table_id" {
  description = "ID of the Transit Gateway Route Table"
  #type        = string
  #default = null
}


variable "subnet_ids" {
  description = "List of subnet IDs to associate with the Transit Gateway"
  #type        = list(string)
  default = null
}

variable "transit_gateway_attachment_id" {
  default = null
}

variable "aws_ec2_transit_gateway_vpc_attachment" {
  default = null
}

variable "aws_ec2_transit_gateway_vpn_attachment" {
  #default = null
  #type = string
  #   type        = object({
  #   id = string
  #   # other attributes if applicable
  # })
}

variable "transit_gateway_route_table_id" {

#default = null 
}

# variable "route_table_id" {
#   default = null
# }






