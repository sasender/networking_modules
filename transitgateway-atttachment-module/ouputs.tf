output "vpc_attachment_id" {
  value = aws_ec2_transit_gateway_vpc_attachment.vpc_attachment.id
}

# output "vpn_attachment_id" {
#   value = data.aws_ec2_transit_gateway_vpn_attachment.example.id
# }

# output "vpn_attachment_info" {
#   value = data.aws_ec2_transit_gateway_vpn_attachment.example
# }


output "transit_gateway_id" {
  value = var.transit_gateway_id
}