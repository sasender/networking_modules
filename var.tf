variable "name" {
  default = null
}

variable "ip_address" {
  default = null
}

variable "region" {
  default = null
}


variable "transit_gateway_id" {
 default =  null 
}